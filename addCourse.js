let createCourse = document.querySelector("#createCourse");

createCourse.addEventListener("submit", (e) => {
  e.preventDefault();

  let adminUser = localStorage.getItem("isAdmin");

  let name = document.querySelector("#courseName").value;
  let description = document.querySelector("#courseDescription").value;
  let price = document.querySelector("#coursePrice").value;

  if (
    name !== "" &&
    description !== "" &&
    price !== "" &&
    adminUser === "true"
  ) {
    fetch("http://localhost:4000/courses", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          alert("New course has been successfully added");
          // console.log(data);
          window.location.replace("./courses.html");
        } else {
          alert("Error: Something went wrong.. Please try again");
        }
      });
  } else {
    alert("Error: Please check your course details and try again");
  }
});
